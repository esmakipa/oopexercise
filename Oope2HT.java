// Otetaan käyttöön File
import java.io.File;
// Otetaan käyttöön Kayttoliittyma
import harjoitustyo.Kayttoliittyma;
// Otetaan käyttöön Vakiot
import harjoitustyo.Vakiot;

/**
 * Ohjelman ajoluokka, sisältä main()-metodin. Luo käyttöliittymän ja käynnistää
 * ohjelman pääsilmukan.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 * <p>
 * @version 4.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class Oope2HT {

    /**
     * Komentoriviargumentteina annetaan tekstitiedostot käsiteltävää
     * dokumenttikokelmaa ja sulkusanoja varten.
     * <p>
     * Tässä ohjelmassa käsitetävät tekstitiedostot (.txt):
     * jokes_[xxxxx].txt (vitsikokoelma, alkaa sanalla "jokes_")
     * tai
     * news_[xxxxx].txt (uutiskokoelma, alkaa sanalla "news_")
     * sekä
     * stop_words[xxxxx].txt (sulkusanat, alkaa sanoilla "stop_words")
     *
     * @param args komentoriviargumentit (dokumenttikokoelma ja sulkusanat).
     */
    public static void main(String[] args) {

        // Toivotetaan käyttäjä tervetulleeksi ohjelmaan.
        System.out.println("Welcome to L.O.T.");

        // Komentoriviargumentteja tulee antaa kaksi, muutoin tulostetaan
        // virheilmoitus ja ohjelman suoritus loppuu.
        if (args.length !=  2) {

            // Komentoriviargumenttien lukumäärä ei ole oikea.
            System.out.println("Wrong number of command-line arguments!");
            System.out.println("Program terminated.");
            System.exit(0);

        }

        // Tiedostot komentoriviargumenteista.
        File documentFile = new File(args[0]);
        File stopWordsFile = new File(args[1]);

        // Tarkistetaan, että komentoriviargumentteina annetut tiedostot ovat
        // olemassa, muutoin tulostetaan virheilmoitus ja ohjelman suoritus loppuu.
        if ((documentFile.exists() && documentFile.getName().contains(Vakiot.UUTISET)) ||
            (documentFile.exists() && documentFile.getName().contains(Vakiot.VITSIT)) &&
            (stopWordsFile.exists() && stopWordsFile.getName().contains(Vakiot.SULKUSANAT))) {

            // Luodaan käyttöliittymä ja käynnistetään ohjelma.
            // Parametreina komentoriviargumentteina saatujen tiedostot..
            new Kayttoliittyma(documentFile, stopWordsFile).kaynnista();

        }
        else {

            // Tiedosto puuttuu tai tiedoston nimi ei täsmää.
            System.out.println("Missing file!");
            System.out.println("Program terminated.");
            System.exit(0);

        }
    }
}
