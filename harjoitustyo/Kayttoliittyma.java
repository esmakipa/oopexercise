package harjoitustyo;

// Otetaan käyttöön luokat pakkauksesta harjoitustyo.dokumentit
import harjoitustyo.kokoelma.Kokoelma;
import harjoitustyo.dokumentit.*;
// Otetaan käyttöön File
import java.io.File;
// Otetaan käyttöön Scanner
import java.util.Scanner;

/**
 * Käyttöliittymäluokka. Huolehtii ohjelman pääsilmukasta, kaikki käyttäjän antamat
 * komennot luetaan käyttöliittymässä.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 8.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class Kayttoliittyma {

    /*
     * Attribuutit
     *
     * lukija, komentojen lukemiseen, Scanner-tyyppinen
     * kokoelma, dokumenttikokoelma, Kokelma-tyyppinen
     * dokumenttiTiedosto, ladattavan dokumenttitiedosto, File-tyyppinen
     * sulkusanatTiedosto, sulkusanat sisältävän tiedosto, File-tyyppinen
     */

    /** Luodaan lukija käyttäjän komentojen lukemiseen. */
    private Scanner lukija = new Scanner(System.in);
    /** Luodaan kokoelma. */
    private Kokoelma kokoelma = new Kokoelma();
    /** Dokumentteja sisältävän tiedoston nimi. */
    private File dokumentitTiedosto;
    /** Sulkusanat sisältävän tiedoston nimi. */
    private File sulkusanatTiedosto;
    /** Komentojen kaiutuksen totuusarvo. */
    private boolean kaiutus = false;

    /*
     * Luokan parametrillinen rakentaja.
     */

    /**
     * Alustaa dokumenttiTiedosto- ja sulkusanatTiedosto-attribuutit parametreina
     * saaduilla arvoilla. Lukee tiedostojen tiedot ohjelmaan.
     *
     * @param dokumentitTiedosto dokumentit sisältävä tiedosto
     * @param sulkusanatTiedosto sulkusanat sisältävä tiedosto
     */
    public Kayttoliittyma(File dokumentitTiedosto, File sulkusanatTiedosto) {
        this.dokumentitTiedosto = dokumentitTiedosto;
        this.sulkusanatTiedosto = sulkusanatTiedosto;
        kokoelma().lataaDokumentit(dokumentitTiedosto);
        kokoelma().lataaSulkusanat(sulkusanatTiedosto);
    }

    /*
     * Aksessorit.
     */

    /**
     * Palauttaa lukijan.
     *
     * @return lukija-attribuutin arvo.
     */
    public Scanner lukija() {

        return lukija;

    }

    /**
     * Palauttaa kokoelman.
     *
     * @return kokoelma-attribuutin arvo.
     */
    public Kokoelma kokoelma() {

        return kokoelma;

    }

    /**
     * Palauttaa dokumentit sisältävän tiedoston.
     *
     * @return dokumentitTiedosto-attribuutin arvo.
     */
    public File dokumentitTiedosto() {

        return dokumentitTiedosto;

    }

    /**
     * Palauttaa sulkusanat sisältävän tiedoston.
     *
     * @return sulkusanatTiedosto-attribuutin arvo.
     */
    public File sulkusanatTiedosto() {

        return sulkusanatTiedosto;

    }

    /**
     * Palauttaa tiedon kaiutetaanko annetut komennot.
     *
     * @return kaiutus-attribuutin arvo.
     */
    public boolean kaiutus() {

        return kaiutus;

    }

    /**
     * Asettaa tiedon kaiutetaanko annetut komennot.
     *
     * @param kaiutus-attribuutin arvo.
     */
    public void kaiutus(boolean kaiutus) {

        this.kaiutus = kaiutus;

    }

    /*
     * Metodit.
     */

    /**
     * Käynnistää ohjelman pääsilmukan.
     */
    public void kaynnista() {

        while (true) {

            // Tulostetaan käyttäjälle kehote komennon antamiseksi.
            System.out.println("Please, enter a command:");

            // Luetaan käyttäjän antama komento.
            String komento = lukija().nextLine();

            // Komento kaiutetaan, jos kaiutus-attribuutin arvo true.
            if (kaiutus() || komento.equals("echo")) {

                System.out.println(komento);

            }

            // Ohjelma lopetetaan, jos käyttäjä antaa komennon 'quit'
            if (komento.equals("quit")) {

                break;

            }

            /*
             * print
             *
             * Tulostetaan annetun tunnisteen mukaisen dokumentin tai kaikkien
             * dokumenttien merkkijonoesitykset:
             *
             * print (tulostaa kaikkien dokumenttien merkkijonoesitykset)
             * print x (tulostaa tunnisteen x omaavan dokumentin merkkijonoesityksen)
             */
            else if (komento.split("\\s+")[0].trim().equals("print") && komento.split("\\s+", 2).length <= 2) {

                // Tulostetaan annetun tunnisteen mukaisen dokumentin merkkijonoesitys.
                if (komento.split("\\s+", 2).length == 2) {

                    System.out.print(kokoelma().tulostaDokumentti(komento.split("\\s+", 2)[1].trim()));

                } else {

                    // Tulostetaan kaikkien dokumenttien merkkijonoesitykset.
                    System.out.print(kokoelma().tulostaDokumentti(""));

                }
            }

            /*
             * add
             *
             * Lisätään uusi dokumentti listaan:
             *
             * add x///yy///zzz (lisää uuden dokumentin listaan, x = tunniste,
             *                   yy = laji (vitsi) tai päivämäärä (uutinen),
             *                   zzz = teksti)
             */
            else if (komento.split("\\s+")[0].trim().equals("add") && komento.split("\\s+", 2).length == 2) {

                try {
                    // Lisätään dokumentti kokoelmaan.
                    kokoelma().lisää(kokoelma().muodostaDokumentti(komento.split("\\s+", 2)[1].trim()));

                } catch (IllegalArgumentException iae) {

                    System.out.println("Error!");
                }
            }

            /*
             * find
             *
             * Tulostetaan niiden dokumenttien tunnisteet, joiden tekstiosasta löytyy:
             * find-komennon parametreina annetut sanat:
             *
             * find x, y, z,... (tulostaa dokumenttien tunnisteet, joiden
             *                   tekstiosasta löytyy sanat x, y, z,...)
             */
            else if (komento.split("\\s+")[0].trim().equals("find") && komento.split("\\s+", 2).length > 1) {

                // TUlostetaan hakusanat sisältävien dokumenttien tunnisteet.
                System.out.print(kokoelma().etsiDokumentit(komento.split("\\s+", 2)[1].trim()));

            }

            /*
             * remove
             *
             * Poistetaan kokoelmasta annetun tunnisteen mukainen dokumentti:
             *
             * remove x (poistaa tunnisteen x omaavan dokumentin)
             */
            else if (komento.split("\\s+")[0].trim().equals("remove") && komento.split("\\s+", 2).length == 2) {

                // Poistetaan dokumentti.
                System.out.print(kokoelma().poistaDokumentti(komento.split("\\s+", 2)[1].trim()));

            }

            /*
             * polish
             *
             * Kokoelman esikäsittely, käyttäjän antamien välimerkkien poistaminen,
             * kaikkien kirjainten muuttaminen pieniksi ja sulkusanojen
             * poistaminen dokumenteista:
             *
             * polish [,.:?"'...] (poistaa dokumenttien tekstiosasta annetut välimerkit,
             *                     muuttaa kirjaimet pieniksi ja poistaa dokumenteista
             *                     sulkusanat tiedonhaun helpottamiseksi)
             */
            else if (komento.split("\\s+")[0].trim().equals("polish") && komento.split("\\s+").length == 2) {

                // Siivotaan dokumentti.
                System.out.print(kokoelma().siivoaDokumentit(komento.split("\\s+", 2)[1].trim()));

            }

            /*
             * reset
             *
             * Perutaan kaikki kokoelmaan tehdyt muutokset ja ladataan dokumentit
             * uudelleen ohjelmaan dokumentit sisältävästä tekstitiedostosta:
             *
             * reset (perutaan tehdyt muutokset ja palautetaan alkutilanne)
             */
            else if (komento.split("\\s+")[0].trim().equals("reset") && komento.split("\\s+", 2).length == 1) {

                // Resetoidaan.
                kokoelma().lataaDokumentit(dokumentitTiedosto());

            }

            /*
             * echo
             *
             * Asetetaan ja poistetaan komentojen kaiutus:
             *
             * echo (asetetaan ja poistetaan komentojen kaiutus)
             */
            else if (komento.split("\\s+")[0].trim().equals("echo")) {

                // Vaihdetaan kaiutus-attribuutin arvo (true -> false tai false -> true).
                if (kaiutus()) {

                    // Komentojen kaiutus pois päältä.
                    kaiutus(false);

                } else {

                    // Komentojen kaiutus päälle.
                    kaiutus(true);

                }
            }

            else {

                // Virheellinen komento.
                System.out.println("Error!");

            }
        }

        System.out.println("Program terminated.");
    }
}
