package harjoitustyo;

/**
 * Luokka vakioille.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 3.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public final class Vakiot {

    /*
     * Vakiot.
     */

    /**
     * Merkkijonoerotin dokumentin tunnisteen, päivämäärän tai lajin
     * sekä tekstin erottamiseen.
     */
    public static final String EROTIN = "///";
    /** Sulkusanat-tiedoston tiedostonimen alku. */
    public static final String SULKUSANAT = "stop_words";
    /** Uutiset-tiedoston tiedostonimen alku. */
    public static final String UUTISET = "news_";
    /** Vitsit-tiedoston tiedostonimen alku. */
    public static final String VITSIT = "jokes_";

    private Vakiot() {

    }
}
