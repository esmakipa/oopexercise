package harjoitustyo.dokumentit;

// Otetaan käyttöön Tietoinen-rajapinta sekä Javan LinkedList (linkitetty lista)
import harjoitustyo.apulaiset.Tietoinen;
import java.util.LinkedList;
// Otetaan käyttöön Arrays
import java.util.Arrays;
// Otetaan käyttöön Vakiot
import harjoitustyo.Vakiot;

/**
 * Abstrakti yliluokka dokumenttien mallintamiseen.
 * <p>
 * Yliluokalla on kaksi dokumenteille yhteistä tietoa: dokumentin tunniste ja
 * dokumentin teksti.
 * <p>
 * Toteuttaa rajapinnat Tietoinen ja Comparable.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 10.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public abstract class Dokumentti implements Tietoinen<Dokumentti>, Comparable<Dokumentti> {

    /*
     * Attribuutit
     *
     * tunniste, dokumentin yksilöivä tunniste, int-tyyppinen
     * teksti, dokumentin teksti, String-tyyppinen
     */

    /** Dokumentin yksilöivä tunniste. */
    private int tunniste;
    /**  Dokumentin teksti. */
    private String teksti;

    /*
     * Luokan parametrillinen rakentaja.
     *
     * Tunnisteen tulee olla positiivinen kokonaisluku ja teksti ei saa olla null
     * tai tyhjä, muuten heitetään IllegalArgumentException.
     */

    /**
     * Alustaa tunniste- ja teksti-attribuutit parametreina saaduilla arvoilla.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos tunniste on pienempi kuin 1
     * tai teksti on null tai tyhjä.
     *
     * @param tunniste dokumentin tunniste.
     * @param teksti dokumentin teksti.
     * @throws IllegalArgumentException jos tunniste on pienempi kuin 1 tai teksti on null tai tyhjä.
     */
    public Dokumentti(int tunniste, String teksti) throws IllegalArgumentException {

        // Heitetään poikkeus, jos parametrit eivät ole sallittuja.
        if (tunniste < 1 || teksti == null || teksti == "") {

            throw new IllegalArgumentException();

        } else {

            // Parametrit ovat sallittuja ja ne voidaan asettaa.
            this.tunniste = tunniste;
            this.teksti = teksti;

        }
    }

    /*
     * Aksessorit.
     */

    /**
     * Palauttaa dokumentin tunnisteen.
     *
     * @return tunniste-attribuutin arvo.
     */
    public int tunniste() {

        return tunniste;

    }

    /**
     * Asettaa dokumentin tunnisteen. Heittää poikkeuksen IllegalArgumentException, jos
     * tunniste on pienempi kuin 1.
     *
     * @param tunniste dokumentin tunniste.
     * @throws IllegalArgumentException jos tunniste on pinenmpi kuin 1.
     */
    public void tunniste(int tunniste) throws IllegalArgumentException {

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (tunniste < 1) {

            throw new IllegalArgumentException();

        } else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.tunniste = tunniste;

        }
    }

    /**
     * Palauttaa dokumentin tekstin.
     *
     * @return teksti-attribuutin arvo.
     */
    public String teksti() {

        return teksti;

    }

    /**
     * Asettaa dokumentin tekstin. Heittää poikkeuksen IllegalArgumentException,
     * jos teksti on null tai tyhjä.
     *
     * @param teksti dokumentin teksti.
     * @throws IllegalArgumentException jos teksti on null tai tyhjä.
     */
    public void teksti(String teksti) throws IllegalArgumentException {

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (teksti == null || teksti == "") {

            throw new IllegalArgumentException();

        } else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.teksti = teksti;

        }
    }

    /*
     * Metodit.
     */

    /**
     * Korvaa Comparable-rajapinnassa määritellyn equals-metodin.
     * <p>
     * Vertailee dokumenttien tunnisteita.
     *
     * @param verrattava Object-tyyppinen olio.
     */
    @Override
    public boolean equals(Object verrattava) {

        // Jos muuttujat sijaitsevat samassa paikassa, ovat ne samat.
        if (this == verrattava) {

            return true;

        }

        // Oliot eivät ole samat, jos verrattava ei ole Dokumentti-tyyppinen.
        if (!(verrattava instanceof Dokumentti)) {

            return false;

        }

        // Muutetaan Object-tyyppinen verrattava-olio Dokumentti-tyyppiseksi
        // verrattavaDokumentti-olioksi
        Dokumentti verrattavaDokumentti = (Dokumentti) verrattava;

        // Jos olioiden tunnisteiden arvot ovat samat, oliot ovat samat.
        if (tunniste() == verrattavaDokumentti.tunniste) {

            return true;

        }

        // Muutoin oliot eivät ole samat.
        return false;
    }

    /**
     * Korvaa Comparable-rajapinnassa määritellyn compareTo()-metodin. Metodin parametrina
     * on Dokumentti-tyyppinen dokumentti.
     * <p>
     * Päättelee dokumenttien suuruusjärjestyksen vertaamalla dokumenttien tunnisteita.
     *
     * @param dokumentti dokumentin tunniste, Object-tyyppinen.
     */
    @Override
    public int compareTo(Dokumentti dokumentti) {

        // Verrattavien dokumenttien tunnisteet ovat samat.
        if (tunniste() == dokumentti.tunniste()) {

            return 0;

        // Verrattavan (parametri) annetun dokumentin tunniste on pienempi.
    } else if (tunniste() > dokumentti.tunniste()) {

            return 1;

        // Verrattavan (parametri) annetun dokumentin tunniste on suurempi.
        } else {

            return -1;

        }
    }

    /**
     * Korvaa Tietoinen-rajapinnassa määritellyn sanatTäsmäävät()-metodin. Metodin parametrina
     * on dokumenteista haettavat hakusanat. Metodi tutkii sisältääkö dokumentin teksti kaikki
     * siitä haettavat sanat.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos hakusanat on null tai tyhjä.
     *
     * @param hakusanat lista dokumentin tekstistä haettavia sanoja.
     * @return true, jos kaikki listan sanat löytyvät dokumentista. Muuten palautetaan
     * false.
     * @throws IllegalArgumentException ,jos sanalista on tyhjä tai se on null.
     */
    @Override
    public boolean sanatTäsmäävät(LinkedList<String> hakusanat) throws IllegalArgumentException {

        // Alustetaan totuusarvo loytyi.
        boolean loytyi = true;

        // Muodostetaan dokumentin tekstiosan sanoista taulukko.
        String[] tekstinOsat = teksti().split(" ");

        // Jos parametri on null tai tyhjä, heitetään IllegalArgumentException.
        if ((hakusanat == null || hakusanat.isEmpty())) {

            throw new IllegalArgumentException();

        } else {

            // Tutkitaan etsittävät sanat dokumentin tekstiosasta.
            for (String hakusana: hakusanat) {

                // Muunnetaan tekstinOsat-taulukko listaksi vertailun suorittamiseksi.
                if (!Arrays.asList(tekstinOsat).contains(hakusana)) {

                    // Etsittävää sanaa ei löydy dokumentin tekstiosasta.
                    loytyi = false;

                    // Hakusanojen etsintä tästä dokumentista voidaan lopettaa.
                    break;

                }
            }
        }

        // Palautetaan totuusarvo, löytyikö dokumentista vaaditut sanat
        // loytyi (true), ei löytynyt (false)
        return loytyi;
    }

    /**
     * Korvaa Tietoinen-rajapinnassa määritellyn siivoa()-metodin. Metodin parametreina
     * ovat dokumenteista poistettavat sulkusanat ja välimerkit. Metodi poistaa ensin
     * annetut välimerkit, muuttaa kirjaimet pieniksi kirjaimiksi ja lopuksi poistaa
     * annetut sulkusanat.
     * <p>
     * Muokkaa dokumenttia siten, että tiedonhaku on helpompaa ja tehokkaampaa.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos sulkusanat tai välimerkit ovat tyhjiä
     * tai ne ovat null.
     *
     * @param sulkusanat lista dokumentin tekstistä poistettavia sanoja.
     * @param välimerkit dokumentin tekstistä poistettavat välimerkit merkkijonona.
     * @throws IllegalArgumentException jos jompikumpi parametri on null tai tyhjä.
     */
    @Override
    public void siivoa(LinkedList<String> sulkusanat, String välimerkit) throws IllegalArgumentException {

        // Jos jompikupmi parametreista on null tai tyhjä, heitetään IllegalArgumentException
        if ((sulkusanat == null || sulkusanat.isEmpty()) ||  (välimerkit == null || välimerkit.isEmpty())) {

            throw new IllegalArgumentException();

        } else {

            // Poistetaan parametrina annetun välimerkit-merkkijonon mukaiset välimerkit.
            for (String merkki: välimerkit.split("")) {

                //teksti(teksti().replace(merkki, "").trim());
                teksti(teksti().replace(merkki, ""));

            }

            // Muutetaan kirjaimet pieniksi.
            teksti(teksti().toLowerCase());

            // Muodostetaan dokumentin tekstiosan sanoista taulukko.
            String[] tekstinOsat = teksti().split(" ");

            // Poistetaan dokumentin tekstistä sulkusanat.
            for (String sulkusana: sulkusanat) {

                // Käydään tekstin osien taulukko läpi ja verrataan kutakin sanaa
                // sulkusnaan. Sana poistetaan tekstistä, jos se vastaa sulkusanaa.
                for (int i = 0; i < tekstinOsat.length; i++) {

                    if (tekstinOsat[i].equals(sulkusana)) {

                        tekstinOsat[i] = "";

                    }
                }

                // Yhdistetään sulkusanoista siivottu tekstitaulukko takaisin
                // merkkijonoksi ja dokumentin tekstiosaksi.
                teksti(String.join(" ", tekstinOsat));

            }

            // Poistetaan dokumentin tekstistä mahdolliset ylimääräiset tyhjät.
            teksti(teksti().replaceAll("\\s+", " ").trim());

        }
    }

    /**
     * Korvaa toString()-metodin.
     * <p>
     * Palauttaa tunnisteen ja tekstin, erottimena käytetään "///".
     *
     * @return merkkijono, jossa tunniste ja teksti on erotettu käyttäen erotinta.
     */
     @Override
     public String toString() {

         return tunniste() + Vakiot.EROTIN + teksti();

     }
}
