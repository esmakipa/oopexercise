package harjoitustyo.dokumentit;

// Otetaan käyttöön Vakiot
import harjoitustyo.Vakiot;

/**
 * Konkreettinen luokka kokoelman (vitsit) mallintamiseen. Peritty abstraktista
 * yliluokasta Dokumentti. Vitsi-luokan oma tieto on String-tyyppinen laji-attribuutti.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 6.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class Vitsi extends Dokumentti {

    /*
     * Attribuutti.
     *
     * laji, vitsin laji, String-tyyppinen
     */

    /** Vitsin laji. */
    private String laji;

    /*
     * Luokan parametrillinen rakentaja.
     *
     * Kutsutaan yliluokan rakentajaa parametreille tunniste ja teksti.
     * Laji ei saa olla tyhjä tai null, muutoin heitetään IllegalArgumentException.
     */

    /**
     * Alustaa laji-attribuutin parametrina saadulla arvolla.
     * <p>
     * Kutsuu yliluokan rakentajaa parametreille tunniste ja teksti.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos laji on null tai tyhjä.
     *
     * @param tunniste dokumentin tunniste.
     * @param laji vitsin laji.
     * @param teksti dokumentin teksti.
     */
    public Vitsi(int tunniste, String laji, String teksti) throws IllegalArgumentException {

        // Kutsutaan yliluokan rakentajaa.
        super(tunniste, teksti);

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (laji == null || laji.equals("")) {

            throw new IllegalArgumentException();

        } else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.laji = laji;

        }
    }

    /*
     * Aksessorit.
     */

    /**
     * Palauttaa vitsin lajin.
     *
     * @return laji-attribuutin arvo.
     */
    public String laji() {

        return laji;

    }

    /**
     * Asettaa vitsin lajin.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos laji on null tai tyhjä.
     *
     * @param laji vitsin laji.
     */
    public void laji(String laji) throws IllegalArgumentException {

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (laji == null || laji.equals("")) {

            throw new IllegalArgumentException();

        }
        else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.laji = laji;

        }
    }

    /*
     * Metodit.
     */

    /**
     * Korvaa toString()-metodin.
     * <p>
     * Palauttaa tunnisteen, lajin ja tekstin toisistaan erotettuna, erottimena käytetään "///".
     *
     * @return merkkijono, jossa tunniste, laji ja teksti on erotettu käyttäen erotinta.
     */
    @Override
    public String toString() {

        return super.tunniste() + Vakiot.EROTIN + laji() + Vakiot.EROTIN + super.teksti();

    }
}
