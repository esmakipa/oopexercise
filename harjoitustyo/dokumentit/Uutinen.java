package harjoitustyo.dokumentit;

// Otetaan käyttöön Javan LocalDate ja DataTimeFormatter
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
// Otetaan käyttöön Vakiot
import harjoitustyo.Vakiot;

/**
 * Konkreettinen luokka kokoelman (uutiset) mallintamiseen. Peritty abstraktista
 * yliluokasta Dokumentti. Uutinen-luokan oma tieto on LocalDate-tyyppinen
 * päivämäärä-attribuutti.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 8.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class Uutinen extends Dokumentti {

    /*
     * Attribuutti.
     *
     * päivämäärä, uutisen päivämäärä, LocalDate-tyyppinen
     */

    /** Uutisen päivämäärä. */
    private LocalDate päivämäärä;

    /*
     * Luokan parametrillinen rakentaja.
     *
     * Kutsutaan yliluokan rakentajaa parametreille tunniste ja teksti.
     * Päivämäärä ei saa olla null, muutoin heitetään IllegalArgumentException.
     */

    /**
     * Alustaa päivämäärä-attribuutti parametrina saadulla arvolla.
     * <p>
     * Kutsuu yliluokan rakentajaa parametreille tunniste ja teksti.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos päivämäärä on null.
     *
     * @param tunniste dokumentin tunniste.
     * @param päivämäärä uutisen päivämäärä.
     * @param teksti dokumentin teksti.
     */
    public Uutinen(int tunniste, LocalDate päivämäärä, String teksti) throws IllegalArgumentException {

        // Kutsutaan yliluokan rakentajaa.
        super(tunniste, teksti);

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (päivämäärä == null) {

            throw new IllegalArgumentException();

        } else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.päivämäärä = päivämäärä;

        }
    }

    /*
     * Aksessorit.
     */

    /**
     * Palauttaa uutisen päivämäärän.
     *
     * @return päivämäärä-attribuutin arvo.
     */
    public LocalDate päivämäärä() {

        return päivämäärä;

    }

    /**
     * Asettaa uutisen päivämäärän.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos päivämäärä on null.
     *
     * @param päivämäärä uutisen päivämäärä.
     */
    public void päivämäärä(LocalDate päivämäärä) throws IllegalArgumentException {

        // Heitetään poikkeus, jos parametri ei ole sallittu.
        if (päivämäärä == null) {

            throw new IllegalArgumentException();

        } else {

            // Parametri on sallittu ja se voidaan asettaa.
            this.päivämäärä = päivämäärä;

        }
    }

    /*
     * Metodit.
     */

    /**
     * Korvaa toString()-metodin.
     * <p>
     * Palauttaa tunnisteen, päivämäärän ja tekstin toisistaan erotettuna,
     * erottimena käytetään "///".
     * <p>
     * Päivämäärän esityksessä käytetty muotoilua.
     *
     * @return merkkijono, jossa tunniste, päivämäärä ja teksi on erotettu käyttäen erotinta.
     */
    @Override
    public String toString() {

        return super.tunniste() +
               Vakiot.EROTIN +
               päivämäärä().format(DateTimeFormatter.ofPattern("d.M.yyyy")) +
               Vakiot.EROTIN + super.teksti();

    }
}
