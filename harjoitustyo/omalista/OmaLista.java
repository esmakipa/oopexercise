package harjoitustyo.omalista;

// Otetaan käyttöön Opperoiva-rajapinta
import harjoitustyo.apulaiset.Ooperoiva;
// Otetaan käyttöön Javan LinkedList
import java.util.LinkedList;
// Otetaan käyttöön ListIterator
import java.util.ListIterator;

/**
 * Javan LinkedList-luokasta peritty OmaLista. Tyyppiparametrina on E.
 * Toteuttaa Ooperoiva-rajapinnan.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 9.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class OmaLista<E> extends LinkedList<E> implements Ooperoiva<E> {

    /*
     * Luokan rakentaja.
     */

    public OmaLista() {

    }

    /*
     * Metodit.
     */

    /**
     * Toteuttaa Ooperoiva-rajapinnan metodin lisää(E uusi). Metodi saa parametrinaan
     * lisättävän elementin.
     * <p>
     * Lisää uuden elemntin listan alkuun tai etsii elementille oikean sijoituspaikan
     * tai lisää elementin listan loppuun.
     *
     * @param uusi lisättävä elementti, E-tyyppinen.
     * @throws IllegalArgumentException jos lisättävä dokumentti on null tai
     * se ei ole vertailtavissa compareTo-metodilla.
     */
    @SuppressWarnings({"unchecked"})
    @Override
    public void lisää(E uusi) throws IllegalArgumentException {

        // Lisättävä dokumentti ei ole null ja on vertailtavissa compareTo-metodilla.
        if (uusi != null && uusi instanceof Comparable) {

            // Alustetaan iteraattori listan läpikäymiseen.
            ListIterator iteraattori = this.listIterator(0);

            // Lista on tyhjä.
            // Lisätään uusi elementti listan alkuun.
            if (this.size() == 0) {

                // Asetetaan uusi elementti listan alkuun.
                this.addFirst(uusi);

            }

            // Lista ei ole tyhjä, listalla elementtejä 1 tai enemmän
            // Tutkitaan, lisätäänkö uusi elementti listan alkuun.
            else if (((Comparable) uusi).compareTo(((Comparable) this.getFirst())) < 0) {

                // Asetetaan uusi elementti listan alkuun.
                this.addFirst(uusi);

            }

            // Uutta elementtiä ei lisätä listan alkuun. Haetaan elementille oikea
            // paikka listassa.
            else {

                // Siirrytään listassa eteenpäin ja haetaan oikea lisäyspaikka.
                while(iteraattori.hasNext()) {

                    // Lisättävä elementti on pienempi kuin seuraava elementti.
                    if (((Comparable) uusi).compareTo(((Comparable) iteraattori.next())) < 0) {

                        // Lisätään uusi elementti listaan.
                        this.add(iteraattori.previousIndex(), uusi);

                        // Listan läpikäynti keskeytetään, paikka lisättävälle elementille
                        // on löytynyt.
                        break;

                    }
                }

                // Lisätään uusi elementti listan loppuun.
                if (!iteraattori.hasNext()) {

                    this.addLast(uusi);

                }
            }

        } else {

            // Heitetään poikkeus.
            // Lisättävä dokumentti on null tai se ei ole vertailtavissa compareTo-metodilla.
            throw new IllegalArgumentException();

        }
    }

    /**
     * Muodostaa tulosteen listan kaikista elementeistä.
     *
     * @return listan kaikkien elementtien merkkijonoesitys.
     */
    @Override
    public String toString() {

        // Alustetaan tulostemerkkijono tyhjäksi.
        String listanSisalto = "";

        // Alustetaan iteraattoti listan läpikäymiseen.
        ListIterator iteraattori = this.listIterator(0);

        // Käydään listan elementit läpi ja lisätään kaikkien elementtien
        // merkkijonoesitys tulostemerkkijonoon.
        while(iteraattori.hasNext()) {

            listanSisalto += iteraattori.next() + System.lineSeparator();

        }

        // Palautetaan tulostemerkkijono.
        return listanSisalto;

    }
}
