package harjoitustyo.kokoelma;

// Otetaan käyttöön Vakiot
import harjoitustyo.Vakiot;
// Otetaan käyttöön rajapinta Kokoava
import harjoitustyo.apulaiset.Kokoava;
// Otetaan käyttöön dokumentit-pakkaus
import harjoitustyo.dokumentit.*;
// Otetaan käyttöön omalista-pakkaus
import harjoitustyo.omalista.*;
// Otetaan kyttöön File
import java.io.File;
// Otetaan käyttöön FileNotFoundException
import java.io.FileNotFoundException;
// Otetaan käyttöön LinkedList
import java.util.LinkedList;
// Otetaan käyttöön Scanner;
import java.util.Scanner;
// Otetaan käyttöön LocalDate
import java.time.LocalDate;
// Otetaan käyttöön DAteTimeFormatter
import java.time.format.DateTimeFormatter;

/**
 * Kokoelmaluokka, joka toteuttaa Kokoava-rajapinnan. Kokoelma toteuttaa
 * komennot OmaListaa käsittelemällä.
 * <p>
 * Harjoitustyö. Olio-ohjelmoinnin perusteet II, kevät 2020.
 *
 * @version 10.0
 * @author Esa Mäkipää (esa.makipaa@tuni.fi)
 */
public class Kokoelma implements Kokoava<Dokumentti> {

    /*
     * Attribuutit
     *
     * dokumentit, dokumentit sisältävä lista, Omalista-tyyppinen
     * sulkusanat, sulkusanat sisältävä lista, LinkedList-tyyppinen
     * dokumenttityyppi, kokoelman dokumenttien tyyppi, String-tyyppinen
     */

    /** Dokumentit sisältävä lista. */
    private OmaLista<Dokumentti> dokumentit;
    /** sulkusanat sisältävä lista. */
    private LinkedList<String> sulkusanat;
    /** Kokoelman dokumentin tyyppi. */
    private String dokumenttityyppi = "";

    /*
     * Luokan rakentaja.
     */

    /**
     * Alustetaan dokumentit- ja sulkusanat-attribuutit.
     */
    public Kokoelma() {

        dokumentit = new OmaLista<Dokumentti>();
        sulkusanat = new LinkedList<String>();

    }

    /*
     * Aksessorit.
     */

    /**
     * Palauttaa dokumenttikokoelman.
     *
     * @return dokumentit-attribuutin arvo.
     */
    public OmaLista<Dokumentti> dokumentit() {

        return dokumentit;

    }

    /**
     * Palauttaa sulkusanat sisältävän listan.
     *
     * @return sulkusanat-attribuutin arvo.
     */
     public LinkedList<String> sulkusanat() {

        return sulkusanat;

    }

    /**
     * Palauttaa dokumentin tyypin.
     *
     * @return dokumenttityyppi-attribuutin arvo.
     */
     public String dokumenttityyppi() {

        return dokumenttityyppi;

    }

    /**
     * Asettaa dokumentin tyypin.
     *
     * @param dokumenttityyppi dokumentin tyyppi.
     */
    public void dokumenttityyppi(String dokumenttityyppi) {

        this.dokumenttityyppi = dokumenttityyppi;

    }

    /*
     * Metodit.
     */

    /**
     * Lisää uuden dokumentin listaan.
     * <p>
     * Heittää poikkeuksen IllegalArgumentException, jos lisättävä dokumentti
     * on null tai se ei ole vertailtavissa compareTo-metodilla.
     *
     * @param uusi lisättävä dokumentti, Dokumentti-tyyppinen.
     * @throws IllegalArgumentException jos lisättävä dokumentti on null tai
     * se ei ole vertailtavissa compareTo-metodilla.
     */
    @Override
    public void lisää(Dokumentti uusi) throws IllegalArgumentException {

        // Lisättävä dokumentti ei ole null ja on vertailtavissa compareTo-metodilla.
        if (uusi != null && uusi instanceof Comparable) {

            // Verrataan lisättävän dokumentin tunnistetta listalla jo olevien
            // dokumenttien tunnisteisiin.
            if (this.hae(uusi.tunniste()) == null) {

                // Dokumentti on uusi ja se voidaan lisätä listaan.
                dokumentit().lisää(uusi);

            } else {

                // Heitetään poikkeus.
                // Listasta löytyy annetun tunnisteen mukainen dokumentti.
                throw new IllegalArgumentException();

            }

        } else {

            // Heitetään poikkeus.
            // Lisättävä dokumentti on null tai se ei ole vertailtavissa compareTo-metodilla.
            throw new IllegalArgumentException();

        }
    }

    /**
     * Hakee annetun tunnisteen omaavaa dokumenttia listasta.
     *
     * @param tunniste haettavan dokumentin tunniste, int-tyyppinen.
     */
    @Override
    public Dokumentti hae(int tunniste) {

        // Asetetaan etsittävän dokumentin arvoksi null.
        Dokumentti dokumentti = null;

        // Etsitään annetun tunnisteen mukaista dokumenttia listasta.
        for (Dokumentti d : dokumentit()) {

            // Annetun tunnisteen mukainen dokumentti löytyy listasta.
            if (d.tunniste() == tunniste) {

                // Asetetaan dokumentin arvoksi listalta löytynyt dokumentti.
                dokumentti = d;

                break;

            }
        }

        // Palautetaan löytynyt dokumentti tai null.
        return dokumentti;
    }

    /**
     * Muodostaa tulosteen ja palauttaa kaikkien dokumenttien tai annetun tunnisteen
     * mukaisen dokumentin merkijonoesitys.
     * <p>
     * Palauttaa virheilmoituksen, jos dokunenttia ei löydy kokoelmasta tai
     * tunniste on epäkelpo.
     *
     * @param tunniste tulostettava(t) dokumentit, String-tyyppinen.
     * @return dokumentin tai dokumnettien merkkijonoesitykset tai virheilmoitus.
     */
    public String tulostaDokumentti(String tunniste) {

        // Alustetaan muodostettava tulostemerkkijono tyhjäksi.
        String tuloste = "";

        // Muodostetaan tulostemerkkijono listan kaikista dokumenteista.
        if (tunniste.equals("") && dokumentit().size() >= 0) {

            // Listan dokumenttien merkkijonoesitykset kerätään tulosteeseen.
            tuloste += dokumentit();

        }
        else {

            // Komennon parametrina on annettu dokumentin tunniste.
            try {

                // Tutkitaan, onko annettu tunniste int-tyyppinen ja suurempi kuin 0.
                if (Integer.parseInt(tunniste) > 0) {

                    // Haetaan listasta annetun tunnisteen mukaista dokumenttia.
                    if (this.hae(Integer.parseInt(tunniste)) != null) {

                        // Dokumentti löytyy listasta, dokumentin merkkijonoesitys lisätään tulosteeseen
                        tuloste += this.hae(Integer.parseInt(tunniste)) + System.lineSeparator();

                    } else {

                        // Annetulla tunnisteella ei löytynyt dokumenttia.
                        tuloste = "Error!\n";

                    }

                } else {

                    // Annettu tunniste on < 1 (negatiivinen tai 0).
                    tuloste = "Error!\n";

                }

            } catch (NumberFormatException e) {

                // Annettu tunniste ei ole int-tyyppinen.
                tuloste = "Error!\n";

            }
        }

        // Palautetaan muodostettu tuloste käyttöliittymään.
        return tuloste;
    }

    /**
     * Etsi annettuja sanoja dokumentista.
     * <p>
     * Palauttaa hakusanat sisältävien dokumenttien tunnisteet merkkijonoesityksenä.
     *
     * @param etsittavat etsittävät sanat sisältävä merkkijono, String-tyyppinen.
     * @return dokumnettien tunnisteiden merkkijonoesitykset tai virheilmoitus.
     */
    public String etsiDokumentit(String etsittavat) {

        // Alustetaan muodostettava tulostemerkkijono tyhjäksi.
        String tuloste = "";

        // Alustetaan linkitetty lista.
        LinkedList<String> hakusanat = new LinkedList<String>();

        // Muodostetaan etsittävistä sanoista taulukko.
        String etsittavatSanat[] = etsittavat.split(" ");

        // Muodostetaan etsittävistä sanoista linkitetty lista.
        for (String etsittavaSana: etsittavatSanat) {

            hakusanat.add(etsittavaSana);

        }

        try {

            for (Dokumentti d : dokumentit()) {

                // Tutkitaan sisältääkö dokumentti annetut hakusanat.
                if (d.sanatTäsmäävät(hakusanat)) {

                    // Lisätään dokumentin tunniste tulosteeseen.
                    tuloste += d.tunniste() + System.lineSeparator();

                }
            }

        } catch (Exception e) {

            tuloste = "Error!\n";

        }

        // Palautetaan muodostettu tuloste käyttöliittymään.
        return tuloste;
    }

    /**
     * Poistaa annetun tunnisteen omaavan dokumentin listasta.
     * <p>
     * Palauttaa virheilmoituksen, jos dokumenttia ei löydy tai tunniste on epäkelpo.
     *
     * @param tunniste poistettavan dokumentin tunniste, String-tyyppinen.
     * @return virheilmoitus.
     */
    public String poistaDokumentti(String tunniste) {

        // Alustetaan muodostettava tulostemerkkijono tyhjäksi.
        String tuloste = "";

        // Komennon parametrina on annettu dokumentin tunniste.
        try {

            // Tutkitaan, onko annettu tunniste int-tyyppinen.
            if (Integer.parseInt(tunniste) > 0) {

                // Haetaan listasta annetun tunnisteen mukaista dokumenttia.
                if (this.hae(Integer.parseInt(tunniste)) != null) {

                    // Dokumentti löytyy listasta, poistetetaan dokumentti.
                    dokumentit().remove(this.hae(Integer.parseInt(tunniste)));

                } else {

                    // Annetulla tunnisteella ei löytynyt dokumenttia.
                    tuloste = "Error!\n";

                }

            } else {

                // Annettu tunniste on < 1 (negatiivinen tai 0).
                tuloste = "Error!\n";

            }

        } catch (NumberFormatException e) {

            // Annettu tunniste ei ole int-tyyppinen
            tuloste = "Error!\n";

        }

        // Palautetaan muodostettu tuloste käyttöliittymään.
        return tuloste;
    }

    /**
     * Siivoaa annetut merkit dokumentista.
     * <p>
     * Palauttaa virheilmoituksen, jos sulkusanat on null tai tyhjä tai merkit
     * on null tai tyhjä.
     *
     * @param merkit poistettavat merkit sisältävä merkkijono, String-tyyppinen.
     * @return virheilmoitus.
     */
    public String siivoaDokumentit(String merkit) {

        // Alustetaan muodostettava tulostemerkkijono tyhjäksi.
        String tuloste = "";

        try {

            // Siivotaan listan dokumenteista annetut merkit.
            for (Dokumentti d : dokumentit()) {

                d.siivoa(sulkusanat(), merkit);

            }

        } catch (IllegalArgumentException iae) {

            tuloste = "Error!\n";

        }

        // Palautetaan muodostettu tuloste käyttöliittymään.
        return tuloste;
    }

    /**
     * Muodostaa dokumentti-olion käyttäjän antamasta komennosta.
     * <p>
     * Palautettaa merkkijonoesityksestä muodostetun Dokumentti-tyyppisen dokumentin.
     *
     * @param teksti lisättävän dokumentin merkkijonoesitys käyttäjän antamasta
     * komennosta, String-tyyppinen.
     * @return muodostettu dokumentti, Dokumentti-tyyppinen.
     */
    public Dokumentti muodostaDokumentti(String teksti) {

        // Alustetaan muodostettava tulostemerkkijono tyhjäksi.
        Dokumentti dokumentti = null;

        // Pilkotaan parametri teksti osiin. Erotin on "///". Riviltä tulee löytyä kolme osaa:
        // tunniste, laji tai päivämäärä, teksti.

        // Muodostetaan parametrina annetusta tekstista muodostettavan dokumentin
        // osat
        String osat[] = teksti.split(Vakiot.EROTIN);

        // Osia pitää olla kolme (tunniste, laji/päivämäärä ja dokumentin teksti)
        // ja tunnisteen pitää olla suurempi kuin 0.
        if (osat.length == 3 && Integer.parseInt(osat[0]) > 0) {

            // Jos käsiteltävänä on kokoelma uutisia ja päivämäärä on kelvollinen,
            // lisätään uusi uutinen.
            if (dokumenttityyppi().equals("Uutinen")) {

                try {

                    // Tutkitaan voidaanko muodostaa kelvollinen uutisen päivämäärä.
                    LocalDate.parse(osat[1], DateTimeFormatter.ofPattern("d.M.yyyy"));

                } catch (Exception e) {

                    // Syötemerkkijonosta ei voida muodostaa Uutinen-tyyppistä dokumenttia.
                    // Päivämäärä on epäkelpo.
                    throw new IllegalArgumentException();

                }

                dokumentti = (new Uutinen(Integer.parseInt(osat[0]),
                                LocalDate.parse(osat[1], DateTimeFormatter.ofPattern("d.M.yyyy")),
                                osat[2]));

            } else {

                // Tutkitaan voidaanko muodostaa kelvollinen vitsin laji.
                // Laji on kirjaimia ja mahdollisesti tyhjiä merkkejä sisältävä merkkijono.
                if (osat[1].matches("[\\sa-zA-Z]*")) {

                    // Jos käsiteltävänä on kokoelma vitsejä ja laji on kelvollinen,
                    // lisätään uusi vitsi.
                    dokumentti = (new Vitsi(Integer.parseInt(osat[0]), osat[1], osat[2]));

                } else {

                    // Syötemerkkijonosta ei voida muodostaa Vitsi-tyyppistä dokumenttia.
                    // Laji on epäkelpo.
                    throw new IllegalArgumentException();

                }
            }

        } else {

            // Syötemerkkijonosta ei voida muodostaa dokumenttia.
            // Tunniste, laji/päivämäärä ja/tai teksti puuttuvat tai tunniste
            // ei ole kokonaisluku (suurempi kuin 0).
            // Dokumentin tunniste, laji/päivämäärä ovat epäkelpoja.
            throw new IllegalArgumentException();

        }

        // Palautetaan muodostettu dokumentti tai null.
        return dokumentti;
    }

    /**
     * Lukee dokumentit sisältävän tiedoston sisällön listaan.
     *
     * @param tiedosto dokumentit sisältävän tiedosto, File-tyyppinen.
     */
    public void lataaDokumentit(File tiedosto) {

        // Alustetaan tiedoston lukija.
        Scanner tiedostonLukija = null;

        // Tyhjennetään dokumenttilista
        if (dokumentit() != null && dokumentit().size() > 0) {

            // Jos dokumentteja löytyy, lista tyhjennetään.
            dokumentit().clear();

        }

        try {

            // Luodaan dokumenttitiedostoon liittyvä olio ja liitetään lukija tiedostoon.
            tiedostonLukija = new Scanner(tiedosto);

            while (tiedostonLukija.hasNextLine()) {

                // Lisätään uutinen tai vitsi tiedoston nimen perusteella.
                if (tiedosto.getName().contains(Vakiot.UUTISET)) {

                    // Asetetaan dokumenttityypiksi uutinen.
                    dokumenttityyppi("Uutinen");

                } else {

                    // Asetetaan dokumenttityypiksi vitsi.
                    dokumenttityyppi("Vitsi");

                }

                // Muodostetaan dokumentti ja lisätään listaan.
                this.dokumentit.lisää(this.muodostaDokumentti(tiedostonLukija.nextLine()));

            }

            // Suljetaan tiedoston lukija.
            tiedostonLukija.close();

        } catch (FileNotFoundException e) {

            if (tiedostonLukija != null) {

                // Suljetaan tiedoston lukija.
                tiedostonLukija.close();

            }
        }
    }

    /**
     * Lukee sulkusanat sisältävän tiedoston sisällön listaan.
     *
     * @param tiedosto sulkusanat sisältävän tiedosto, File-tyyppinen.
     */
    public void lataaSulkusanat(File tiedosto) {

        // Alustetaan tiedoston lukija.
        Scanner tiedostonLukija = null;

        // Tyhjennetään sulkusanat-lista
        if (sulkusanat() != null && sulkusanat().size() > 0) {

            // Jos sanoja löytyy, lista tyhjennetään.
            sulkusanat().clear();

        }

        try {

            // Luodaan dokumenttitiedostoon liittyvä olio ja liitetään lukija tiedostoon.
            tiedostonLukija = new Scanner(tiedosto);

            while (tiedostonLukija.hasNextLine()) {

                // Luetaan tiedoston rivi ja lisätään sulkusanat-listaan.
                this.sulkusanat().add(tiedostonLukija.nextLine());

            }

            // Suljetaan tiedoston lukija.
            tiedostonLukija.close();

        } catch (FileNotFoundException e) {

            if (tiedostonLukija != null) {

                // Suljetaan tiedoston lukija.
                tiedostonLukija.close();

            }
        }
    }
}
