## Yleistä

Olio-ohjelmoinnin  perusteet II -kurssin harjoitustyö.

Ohjelma on Java-kielellä ohjelmoitu tekstimuotoisia kokoelmia (lajitellut vitsit ja päivämäärän sisältävät uutiset) käsittelevä olioperustainen ohjelma.

Dokumentit luetaan ohjelmaan annetuista tekstitiedostoista. Ohjelmassa kokoelmia mallinnetaan abstraktin yliluokan _Dokumentti_ ja konkreettisten luokkien _Vitsi_ ja _Uutinen_ avulla.

Dokumenttien säilömiseen käytetään Javan _LinkedList_-luokasta perittyä geneeristä _OmaLista_-luokkaa.

## Toiminnot

Tekstimuotoisen käyttöliittymän avulla voidaan tulostaa yksittäisen dokumentin tai kaikkien dokumenttien tiedot, lisätä, poistaa, etsiä dokumentteja ja esikäsitellä dokumenttien tietoa tiedonhaun avuksi. Lisäksi ohjelmassa voidaan ladata dokumenttitiedosto uudelleen ja käynnistää sekä lopettaa komentojen kaiutus.  

## Käyttö

Ohjelman käsittelemät tekstitiedostot (.txt):

- vitsikokoelma (alkaa merkkijonolla "jokes_"): **jokes_**[xxxxx].txt          
- uutiskokoelma (alkaa merkkijonolla "news_"): **news_**[xxxxx].txt           
- sulkusanat (alkaa merkkijonolla "stop_words"): **stop_words**[xxxxx].txt

Ohjelma käynnistetään komentoriviltä (esim):

java Oope2HT _jokes_oldies.txt stop_words.txt_ (vitsikokoelma)

java Oope2HT _news_reuters.txt stop_words.txt_ (uutiskokoelma)

## Linkit

- [Oope2HT.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/Oope2HT.java)
- harjoitustyo-pakkaus
  - apulaiset-pakkaus
    - [Kokoava.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/apulaiset/Kokoava.java)
    - [Ooperoiva.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/apulaiset/Ooperoiva.java)
    - [Tietoinen.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/apulaiset/Tietoinen.java)
  - dokumentit-pakkaus
    - [Dokumentti.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/dokumentit/Dokumentti.java)
    - [Uutinen.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/dokumentit/Uutinen.java)
    - [Vitsi.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/dokumentit/Vitsi.java)
  - kokoelma-pakkaus
    - [Kokoelma.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/kokoelma/Kokoelma.java)
  - omalista-pakkaus
    - [Omalista.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/omalista/OmaLista.java)
  - [Kayttoliittyma.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/Kayttoliittyma.java)
  - [Vakiot.java](https://course-gitlab.tuni.fi/oope2-2020/mmesma/-/blob/master/harjoitustyo/Vakiot.java)
